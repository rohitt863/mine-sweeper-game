import React, { Component } from 'react';
import './App.css';
import MineSweep from './container/MineSweep/MineSweep';
import Layout from './hoc/Layout/Layout';
class App extends Component {
  render() {
    return (
      <div>
          <Layout>
            <MineSweep/>
          </Layout>
      </div>
    );
  }
}

export default App;
