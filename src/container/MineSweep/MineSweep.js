import React,{Component} from 'react';
import Board from '../../component/Board/Board';
import '../../component/UI/Button/Button.css'
import Button from '../../component/UI/Button/Button';
import './MineSweep.css'
class MineSweep extends Component {
    state = {
        arr:[
            [0,0,0],
            [0,0,0],
            [0,0,0]
        ],
        minePuted:false,
        click:true
    }
    
    resetHandler=()=>{
        const arr=[
            [0,0,0],
            [0,0,0],
            [0,0,0]
        ]
        this.setState({arr:arr,click:true});
    }

    putMineHandler=(x,y)=>{
        if(this.state.click){
            const arrValue = this.state.arr 
            if( arrValue[x][y]===1){
                arrValue[x][y]=0;
            }else{
                arrValue[x][y]=1;  
            }
            this.setState({arr:arrValue,minePuted:true});
        }
    }

    countMineHandler=(x,y,resultFinal)=>{
        let countArray=[];
        if(x>0){
            countArray.push(resultFinal[x-1][y]);
        }
        if(y>0){
            countArray.push(resultFinal[x][y-1]);
        }
        if(x<resultFinal.length-1){
            countArray.push(resultFinal[x+1][y]);
        }
        if(y<resultFinal.length-1){
            console.log(x,y)
            countArray.push(resultFinal[x][y+1]);
        }
        if(x<resultFinal.length-1 && y<resultFinal.length-1){
            countArray.push(resultFinal[x+1][y+1]);
        }
        if(x>0 && y<resultFinal.length-1){
            countArray.push(resultFinal[x-1][y+1]);
        }
        if(y>0 && x<resultFinal.length-1){
            countArray.push(resultFinal[x+1][y-1]);
        }
        if(y>0 && x>0){
            countArray.push(resultFinal[x-1][y-1]);
        }
        return countArray;
    }
    addBoardHandler=()=>{
        let size = prompt("Board SIze", "Size");
        if(size===null){
            size=3;
        }
        if(size=>3 && size<=15){
            const brr=[];
        for (let i = 0; i < size; i++) {
            const arr = [];
            for (let j = 0; j < size; j++) {
                arr.push(0);
            }
            brr.push(arr);
        }
        console.log(brr)
        this.setState({arr:brr});
        }else{
            alert("Please insert size between 3 to 15")
        }
    }
    resultHandler=()=>{
        const stateArray=this.state.arr
        const result1 = JSON.stringify(this.state.arr)
        const resultFinal = JSON.parse(result1)
        stateArray.map((item,outerIndex)=>{
            for(let innerIndex in item){
                if(stateArray[outerIndex][innerIndex]===1){
                    resultFinal[outerIndex][innerIndex]=9; 
                }else{
                    let countArray=this.countMineHandler(outerIndex,parseInt(innerIndex),stateArray);    
                    console.log(countArray)         
                    let mine=0;
                    for(let value of countArray){
                        if(value===1){
                            mine++;
                        }
               
                    }              
                    resultFinal[outerIndex][innerIndex]=mine; 
                }
            }
        });
        this.setState({arr:resultFinal,minePuted:false,click:false});

    }
    render () {
        return (
            <div>
                <h2>Please Choose Bord Size</h2>
                <Button clicked={this.addBoardHandler} disabaleVal={true}>Board Size</Button>
                <Board 
                input={this.state.arr}
                onClickMineAdded={this.putMineHandler}
                result={this.resultHandler}
                disabaleVal={this.state.minePuted}
                reset={this.resetHandler}
                />          
            </div>
        );
    }
}

export default MineSweep;