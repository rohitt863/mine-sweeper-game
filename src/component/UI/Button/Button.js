import React from 'react';
import './Button.css'
const button=(props)=>{
    return ( <button disabled={!props.disabaleVal}
        onClick={props.clicked} className='Button'>{props.children}</button>);
   
}

export default button;