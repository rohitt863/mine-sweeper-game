import React from 'react';
import Change from '../Change/Change';
import './Board.css';
import Button from '../UI/Button/Button'
const board=(props)=>{
    let field = props.input.map((item , index) => {    
        return(
            <div>
                {
                    item.map((innerItem, innerIndex) => {
                        return(
                            <Change 
                                key = {innerIndex}
                                x={index}
                                y={innerIndex}
                                arr={props.input}
                                mineAdded={props.onClickMineAdded}
                            ></Change>
                        )
                    })
                }
            </div>
        )
    })
    return (
       <div className='Board'>
            {field}
            <Button 
                clicked={props.result}
                disabaleVal={props.disabaleVal}
            >PLAY</Button>
            <Button 
                clicked={props.reset}
                disabaleVal={!props.disabaleVal}
            >RESET</Button>
       </div>
       
    );
}

export default board;