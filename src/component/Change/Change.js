import React from 'react';
import './Change.css';

const change=(props)=>{
    return (
        <div className='Change'  key={props.x} onClick={() => {props.mineAdded(props.x,props.y)}}>
            <p>{props.arr[props.x][props.y] }</p>
        </div>
    )
}

export default change;